import 'dart:convert';


List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.Nama,
    this.Jenis,
    this.Harga,
    this.PhotoUrl,
    
   
  });

  String Nama;
  String Jenis;
  String Harga;
  String PhotoUrl;
  
  

  Map toJson() => {
        "nama_pakaian": Nama,
        "jenis_pakaian": Jenis,
        "harga_pakaian": Harga,
        "photo_Url_pakaian": PhotoUrl,
      };
}
